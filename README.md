# OpenML dataset: USCrime

https://www.openml.org/d/1089

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Datasets of Data And Story Library, project illustrating use of basic statistic methods, converted to arff format by Hakan Kjellerstrand.
Source: TunedIT: http://tunedit.org/repo/DASL

DASL file http://lib.stat.cmu.edu/DASL/Datafiles/USCrime.html

US Crime

Reference:   Vandaele, W. (1978) Participation in illegitimate activities:  Erlich revisited.  In Deterrence and incapacitation, Blumstein, A., Cohen, J. and Nagin, D., eds., Washington, D.C.:  National Academy of Sciences, 270-335.
Methods:  A Primer, New York:  Chapman & Hall, 11.
Also found in:  Hand, D.J., et al. (1994) A Handbook of Small Data Sets, London:  Chapman & Hall, 101-103.
Authorization:   Contact author
Description:   These data are crime-related and demographic statistics for 47 US states in 1960.  The data were collected from the FBI's Uniform Crime Report and other government agencies to determine how the variable crime rate depends on the other variables measured in the study.
Number of cases:   47
Variable Names:

R:   Crime rate:  # of offenses reported to police per million population
Age:   The number of males of age 14-24 per 1000 population
S:   Indicator variable for Southern states (0 = No, 1 = Yes)
Ed:   Mean # of years of schooling x 10 for persons of age 25 or older
Ex0:   1960 per capita expenditure on police by state and local government
Ex1:   1959 per capita expenditure on police by state and local government
LF:   Labor force participation rate per 1000 civilian urban males age 14-24
M:   The number of males per 1000 females
N:   State population size in hundred thousands
NW:   The number of non-whites per 1000 population
U1:   Unemployment rate of urban males per 1000 of age 14-24
U2:   Unemployment rate of urban males per 1000 of age 35-39
W:   Median value of transferable goods and assets or family income in tens of $
X:   The number of families per 1000 earning below 1/2 the median income

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1089) of an [OpenML dataset](https://www.openml.org/d/1089). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1089/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1089/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1089/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

